// const fs = require('fs')
const path = require('path')
const _ = require('lodash')
const spawn = require('child_process').spawn
const schedule = require('node-schedule')
const mongoose = require('mongoose')
const moment = require('moment')
const CONFIG = require('./lib/config')(__dirname, 'Service-Schedule')
const app = require('./service.js')()
const active = require('./lib/active')

if (!CONFIG.mongodb) {
  scheduleJobs()
} else {
  const optMongoose = {
    socketTimeoutMS: 15000,
    keepAlive: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
  const mongooseUri = `mongodb://${CONFIG.mongodb.uid}:${CONFIG.mongodb.pwd}@${CONFIG.mongodb.host}:${CONFIG.mongodb.port}/${CONFIG.mongodb.database}`
  mongoose.connect(mongooseUri, optMongoose).then(
    () => {
      console.log('mongodb connect successfully')
      active.dbConnected = true
      scheduleJobs()
      app.listen(CONFIG.port, function () {
        console.log('HTTP Server listening on port: %s, in %s mode', CONFIG.port, app.get('env'))
      })
    },
    err => {
      if (err) {
        console.log(err)
        setTimeout(function () {
          process.exit(-1)
        }, 15000)
      }
    }
  )
}

function scheduleJobs () {
  const jobs = CONFIG.jobs
  jobs.forEach(element => {
    schedule.scheduleJob(element.name, element.cron, () => {
      runJob(element)
    })
  })
}

async function runJob (job) {
  const startTime = moment()
  try {
    if (_.isString(job.script)) {
      await processScript(job.script)
    } else if (_.isArray(job.script)) {
      let cwd = job.cwd || __dirname
      for (let i = 0; i < job.script.length; i++) {
        const tmp = job.script[i].split(' ')
        const command = tmp[0]
        const args = _.drop(tmp)
        console.log(job.script[i])
        await processScript(command, args, cwd)
        if (command === 'cd') {
          cwd = (path.isAbsolute(args.join(' '))) ? args.join(' ') : path.join(cwd, args.join(' '))
        }
      }
    }
    active.log({
      name: job.name,
      startTime: startTime,
      endTime: moment(),
      script: job.script,
      status: 'success',
      errMsg: ''
    })
  } catch (error) {
    active.log({
      name: job.name,
      startTime: startTime,
      endTime: moment(),
      script: job.script,
      status: 'failed',
      errMsg: error.stack
    })
  }
}

/**
 * @param {string} command
 * @param {string} args
 * @param {string} cwd
 */
function processScript (command, args, cwd) {
  return new Promise((resolve, reject) => {
    let ls
    if (cwd) {
      ls = spawn(command, args, { cwd: cwd, shell: true })
    } else {
      ls = spawn(command, args, { shell: true })
    }
    ls.stderr.on('data', data => {
      console.error(data.toString())
    })
    ls.stdout.on('data', data => {
      console.log(data.toString())
    })
    ls.on('close', code => {
      if (code !== 0) {
        return reject(new Error('ECODE: ' + code))
      } else {
        return resolve()
      }
    })
  })
}
