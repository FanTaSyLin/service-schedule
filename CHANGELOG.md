# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.3.0](https://117.78.47.33///compare/v0.2.2...v0.3.0) (2021-01-28)


### Features

* 将Service-Schedule改造为服务 ([06cb2cd](https://117.78.47.33///commit/06cb2cdf3a9e14cf52fd00ca325a0936a5ba9978))
* 新增Schedule相关API ([afa255d](https://117.78.47.33///commit/afa255d499baeedbb4db3a405bbf32a706d594ae))

### [0.2.2](https://117.78.47.33///compare/v0.2.1...v0.2.2) (2020-10-20)


### Bug Fixes

* spawn 在linux上运行必须要指定 options.shell = true ([cb2fffb](https://117.78.47.33///commit/cb2fffb3347e41b93ee21f880c0a9a95fed01c4a))

### [0.2.1](https://117.78.47.33///compare/v0.2.0...v0.2.1) (2020-10-20)


### Bug Fixes

* 现在 cd 切换目录后工作目录同时也会重定位到指定目录 ([0cb31be](https://117.78.47.33///commit/0cb31be32e4fbeab89b5d1d8a517780be121589a))

## 0.2.0 (2020-10-16)


### Features

* initial Commit ([d3ca326](https://117.78.47.33///commit/d3ca326469c3fc0e9262f7ee11a9e3422fb87b23))
* 现在script可以支持多shell命令 ([d9d8454](https://117.78.47.33///commit/d9d8454deb53d1de74f2a1a13f37457305a7b84b))
