const mongoose = require('mongoose')
const Schema = mongoose.Schema
const CONFIG = require('../lib/config')()

const TTL = 60 * 60 * 24 * CONFIG.TTL || 90

const ScheduleJobSchema = new Schema({
  name: { type: String },
  startTime: { type: Date },
  endTime: { type: Date },
  status: { type: String },
  script: { type: Object },
  errMsg: { type: String },
  createdAt: { type: Date, default: Date.now, index: { expires: TTL } }
}, {
  toJSON: { virtuals: true }
})

ScheduleJobSchema.methods.initData = function (body) {
  for (const p in body) {
    this[p] = body[p]
  }
}

ScheduleJobSchema.static('findSchema', (args) => {
  return new Promise((resolve, reject) => {
    let query = Model.find(args.condition || {})
    if (args.populate) {
      query = query.populate(args.populate)
    }
    if (args.select) {
      query = query.select(args.select)
    }
    if (args.sort) {
      query = query.sort(args.sort)
    }
    if (args.skip) {
      query = query.skip(args.skip)
    }
    if (args.limit) {
      query = query.limit(args.limit)
    }
    query.exec((err, doc) => {
      if (err) {
        return reject(err)
      }
      const list = []
      doc.forEach(element => {
        list.push(element.toJSON())
      })
      return resolve(list)
    })
  })
})

ScheduleJobSchema.static('updateSchema', (args) => {
  return new Promise((resolve, reject) => {
    Model.updateOne(args.condition, args.updateQuery, (err, raw) => {
      if (err) {
        return reject(err)
      }
      return resolve(raw)
    })
  })
})

ScheduleJobSchema.static('addSchema', (body) => {
  return new Promise((resolve, reject) => {
    const schema = new Model()
    schema.initData(body)
    schema.save((err, doc) => {
      if (err) {
        return reject(err)
      }
      return resolve(doc.toJSON())
    })
  })
})

ScheduleJobSchema.static('deleteSchema', (condition) => {
  return new Promise((resolve, reject) => {
    Model.deleteOne(condition, (err) => {
      if (err) {
        return reject(err)
      }
      return resolve()
    })
  })
})

const Model = mongoose.model('ScheduleJob', ScheduleJobSchema)

module.exports = Model