const ScheduleJobModel = require('../models/schedulejob-model')
const active = {
  dbConnected: false,
  log: (body) => {
    if (!active.dbConnected) {
      return
    }
    ScheduleJobModel.addSchema(body)
  }
}
module.exports = active