# Service-Schedule

定时脚本服务, 根据`cron`设定的时间频次运行`script`中的指令.

如可连接数据库，会记录历史任务的处理状态 记录默认保留90天

## 说明

config.yml 中加入如下配置

不配置mongodb则Service-Schedule只负责定时运行脚本，不会记录运行日志

```yml
Service-Schedule:
  port: 5000
  # time to live 单位：天
  TTL: 90
  mongodb:
    host: '121.36.13.81'
    port: '27071'
    uid: 'shinetekview'
    pwd: '68400145'
    database: 'shinetekview'
  jobs: 
    - name: 'Project Overview'
      cron: '0 */1 * * * *'
      script:
        - cd /Users/fantasylin/Documents/Develop/GitLab/pmsoft/scripts
        - node project-overview-script.js

```

| Field  | Type            | Exp                             | Description                                        |
|--------|-----------------|---------------------------------|----------------------------------------------------|
| name   | string          | 'Download Windy'                | job name                                           |
| script | string/[string] | './download-nwp/gfs.gprb2.1.js' | shell指令             |
| cron   | string          | '42 * * * * *'                  | emit a run event after each execution              |

```txt
*    *    *    *    *    *
┬    ┬    ┬    ┬    ┬    ┬
│    │    │    │    │    │
│    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
│    │    │    │    └───── month (1 - 12)
│    │    │    └────────── day of month (1 - 31)
│    │    └─────────────── hour (0 - 23)
│    └──────────────────── minute (0 - 59)
└───────────────────────── second (0 - 59, OPTIONAL)

# 每20分钟运行一次  0 */20 * * * *
# 每小时的第五分钟运行一次 0 5 * * * *
```

## ？？？

### node-schedule 是同步运行脚本 还是顺序运行？

任务内 是顺序运行shell指令

两个任务间是同步运行

### node-schedule 是到时间就运行 还是会等上次一运行结束在运行

到时间就会开始任务，不过上一次的任务是否结束

### 如何记录下任务的处理情况

* 开始时间
* 结束时间
* 是否成功（失败的话 错误信息）
