const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const compression = require('compression')
module.exports = function () {
  const app = express()
  app.use(compression())
  app.use('/app', express.static(path.join(__dirname, './app')))
  app.use('/', express.static(path.join(__dirname, './app')))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json({ limit: '50mb' }))
  app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header(
      'Access-Control-Allow-Headers',
      'Content-Type,Content-Length, Authorization, Accept,X-Requested-With'
    )
    res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS')
    res.header('X-Powered-By', '3.2.1')
    if (req.method === 'OPTIONS') {
      res.send(200) // 让options请求快速返回
    } else {
      next()
    }
  })
  // =======================此处引入自己的路由文件======================
  app.use('/service', require('./routes/schedule-handler')())
  app.use('/service', require('./routes/other-handler')())
  // ==================================================================
  app.use('*', function (req, res, next) {
    console.error(`not found resources: '${req.originalUrl}'`)
    res.status(404).end()
  })
  app.use(function (err, req, res, next) {
    return res.status(500).send(err.message)
  })
  return app
}
