const _ = require('lodash')
const Router = require('express').Router
const ScheduleJobModel = require('../models/schedulejob-model')
const CONFIG = require('../lib/config')()

module.exports = function () {
  const router = new Router()
  router.route('/schedule').get(getScheduleList)
  return router
}

async function getScheduleList (req, res, next) {
  try {
    const ymlJobs = await getYMLJobs()
    const dbJobs = await getDBJobs()
    const jobs = _.concat([], ymlJobs, dbJobs)
    const jobsRunStatus = await getJobsRunStatus(jobs)
    _.map(jobs, element => {
      const runStatus = _.find(jobsRunStatus, o => {
        return o.name === element.name
      }) || {}
      element.startTime = runStatus.startTime
      element.endTime = runStatus.endTime
      element.status = runStatus.status
      element.errMsg = runStatus.errMsg
      return element
    })
    return res.status(200).json(jobs)
  } catch (err) {
    return next(err)
  }
}

async function getJobsRunStatus (jobs) {
  try {
    const condition = {
      name: {
        $in: _.map(jobs, o => {
          return o.name
        })
      }
    }
    const doc = ScheduleJobModel.findSchema({
      condition: condition,
      sort: { startTime: 'desc' },
      skip: 0,
      limit: 1
    })
    return doc
  } catch (err) {
    throw err
  }
}

function getYMLJobs () {
  return CONFIG.jobs
}

function getDBJobs () {
  return []
}

